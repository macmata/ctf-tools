#!/bin/python

import re

def s2h(string):
    return h(string.encode("hex"))

def h2s(h):
    return h.decode("hex")

def h(string):
    return "".join(map(lambda x : "\\x"+x ,re.findall('..',string)))

def s(string):
    return string.encode('string-escape').replace("\\x","")

def big2litle(string):
   pass

